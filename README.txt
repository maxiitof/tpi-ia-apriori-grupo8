# README #

Repositorio: https://bitbucket.org/maxiitof/tpi-ia-apriori-grupo8

### Requerimientos ###
* PHP v7.0 o superior
* Composer v1.4.2 o superior
* Git
* SO Linux (preferentemente)

### Instalación de paquetes requeridos ###
1. Abrir una terminal del sistema y correr el siguiente comando:

    Para distribuciones basadas en Debian:
        $ sudo apt-get install -y php composer git

    Para distribuciones basadas en Redhat:
        $ sudo dnf install -y php composer git

2. Comprobar la version de php instalada:
    $ php --version

3. En caso de que la version de php no sea la version 7 o superior, por favor actualizar la misma mediante el siguiente comando.

    Para distribuciones basadas en Debian:
        ```
        $ sudo apt-get upgrade
        ```

    Para distribuciones basadas en Redhat:
        ```
        $ sudo dnf upgrade
        ```

### Descarga el repositorio e instalación de dependencias del proyecto.
1. Clonar el repositorio del proyecto en la carpeta del usuario del sistema.

    $ cd ~/
    $ git clone https://bitbucket.org/maxiitof/tpi-ia-apriori-grupo8.git

2. Ingresar al repositorio.

    $ cd ~/tpi-ia-apriori-grupo8

3. Instalar las dependencias del proyecto

    $ composer install

4. Iniciar el servidor web local embebido para servir la aplicación web del proyecto.

    $ php -S localhost:8000 -t src

5. Abrir el navegador web de preferencia y acceder a la URL **http://localhost:8000**

### Iniciar la aplicación web desde el CD de instalación ###

El CD de instalación ya trae todas las dependencias dentro del proyecto por lo tanto
no es necesario usar composer para instalarlas, sin embargo el mismo se utiliza para
generar el autoloader de clases de PHP.

1. Copiar la carpeta del proyecto contenida en el CD de instalación en la carpeta del usuario del sistema.
2. Abrir una terminal del sistema y acceder al directorio donde se encuentra ubicado el proyecto.

    $ cd ~/tpi-ia-apriori-grupo8

3. Iniciar el servidor web local embebido para servir la aplicación web del proyecto.

    $ php -S localhost:8000 -t src

4. Abrir el navegador web de preferencia y acceder a la URL **http://localhost:8000**

5. Hacer uso de la interfaz web.