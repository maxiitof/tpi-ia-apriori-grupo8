<?php
require_once 'vendor/autoload.php';

$transactionsProcessor = new IAFRRe\Apriori\Transaction\Adapter\FilesystemAdapter(__DIR__ . '/db/DatasetTest2.dat');

$transactions = $transactionsProcessor->getTransactions();

$time_start = microtime(true);

$apriori = new \IAFRRe\Apriori\Apriori();
$apriori
    ->setMinSupp(99)
    ->setMinConf(99)
    ->setTransactionCollection($transactions);

$apriori->run();

$time_end = microtime(true);
$time = $time_end - $time_start;

$averageSupport = array_reduce(
    $apriori->itemsFrequency,
    function($carry, $item) {
        $carry += $item->support;
        return $carry;
    },
    0
);
$frequentItemsetsGenerated = count($apriori->frequentSets);
$averageSupport = $averageSupport / count($apriori->itemsFrequency);
$rulesGenerated = count($apriori->rules);

$length = count($apriori->frequentSets);
for ($i = 1; $i <= $length; $i++) {
    echo "F$i = [\n";
    foreach ($apriori->frequentSets[$i] as $item) {
        echo $item;
    }
    echo "]\n";
}

echo "Reglas: \n";
foreach ($apriori->rules as $rule) {
    echo $rule;
}

echo sprintf(
    "Execution Time: %s secs.\nAverage Support: %s.\nTotal Frequent Sets: %s.\nTotal rules: %s.\nSupp: %s.\nConf: %s.\n",
    $time,
    $averageSupport,
    $frequentItemsetsGenerated,
    $rulesGenerated,
    $apriori->getMinSupp(),
    $apriori->getMinConf()
);