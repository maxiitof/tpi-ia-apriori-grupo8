<?php

namespace IAFRRe\Apriori;

use function array_intersect;
use IAFRRe\Apriori\Item\ItemModel;
use IAFRRe\Apriori\Rule\RuleModel;
use IAFRRe\Apriori\Transaction\TransactionModel;
use function ksort;

class Apriori
{
    /**
     * @var float
     */
    public $minConf;

    /**
     * @var float
     */
    public $minSupp;

    /**
     * @var array
     */
    public $itemsFrequency;

    /**
     * @var array
     */
    public $frequentItems;

    /**
     * @var array
     */
    public $frequentSets;

    /**
     * @var array
     */
    public $rules;

    /**
     * @var TransactionModel[]
     */
    protected $transactionCollection;

    public function __construct()
    {
        $this->minConf = 0;
        $this->minSupp = 0;

        $this->transactionCollection = array();
        $this->itemsFrequency = array();
        $this->frequentItems = array();
        $this->frequentSets = array();
        $this->rules = array();
    }

    /**
     * Gets the current minimum confidence value that the algorithm it's using to generate candidates.
     *
     * @return float
     */
    public function getMinConf()
    {
        return $this->minConf;
    }

    /**
     * Sets the minimum confidence the algorithm needs to generate candidates.
     * This percentage value should be a float/integer number between 0-100.
     *
     * @param float $minConf
     */
    public function setMinConf(float $minConf)
    {
        $this->minConf = $minConf / 100;

        return $this;
    }

    /**
     * Gets the current minimum support value that the algorithm it's using to generate candidates.
     *
     * @return float
     */
    public function getMinSupp()
    {
        return $this->minSupp;
    }

    /**
     * Sets the minimum support the algorithm needs to generate candidates.
     * The value should be a float/integer number between 0-100.
     *
     * @param float $minSupp
     */
    public function setMinSupp(float $minSupp)
    {
        $this->minSupp = $minSupp / 100;

        return $this;
    }

    /**
     * Provides access to the collection of transactions
     *
     * @return TransactionModel[]
     */
    public function getTransactionCollection(): array
    {
        return $this->transactionCollection;
    }

    /**
     * Sets the collection of transactions that the algorithm uses to run the data mining process.
     *
     * @param TransactionModel[] $transactionCollection
     */
    public function setTransactionCollection(array $transactionCollection)
    {
        $this->transactionCollection = $transactionCollection;

        return $this;
    }

    /**
     * Gets the length of the transaction set.
     *
     * @return int
     */
    public function getTransactionSetSize()
    {
        return count($this->transactionCollection);
    }

    /**
     * Runs the Apriori algorithm. This method should be invoked from command line preferably.
     */
    public function run()
    {
        $this
            ->calcItemsFrequency()
            ->filterFrequentItems()
            ->generateFrequentSets()
            ->generateRules();
    }

    /**
     * Returns the full set of items within the Transaction's DB that appears at least once.
     * Each item's support is calculated as well.
     *
     * @return ItemModel[]
     */
    public function getItemsFrequency()
    {
        if (empty($this->itemsFrequency)) {
            $this->calcItemsFrequency();
        }

        return $this->itemsFrequency;
    }

    /**
     * Calculates the support and frequency for each item that belongs to the set of transactions in the DB.
     *
     * @return self
     */
    public function calcItemsFrequency()
    {
        // Count items appearance within the transaction set
        foreach ($this->transactionCollection as $transaction) {
            foreach ($transaction as $item) {
                if (isset($this->itemsFrequency[$item])) {
                    $this->itemsFrequency[$item]->count++;
                } else {
                    $frequentItem = new ItemModel();
                    $frequentItem->value = array($item);
                    $frequentItem->count = 1;

                    // Use the item's value as a key to improve search speed within the Items Frequency collection
                    $this->itemsFrequency[$item] = $frequentItem;
                }
            }
        }

        // Reset the associative keys and sort by value
        $this->itemsFrequency = array_values($this->itemsFrequency);
        $this->sort($this->itemsFrequency, 'value');
        $this->calcSupport($this->itemsFrequency);

        return $this;
    }

    /**
     * Sorts collections of item sets (by values) and rules (by precedents) by a given property.
     * @param array $itemSet
     * @param string $property
     */
    public function sort(array &$itemSet, string $property)
    {
        usort($itemSet, function ($a, $b) use ($property) {
            return strnatcmp(implode('-', $a->{$property}), implode('-', $b->{$property}));
        });
    }

    /**
     * Calculates the support for every unique item within the collection of transactions.
     * @param ItemModel[] $itemSet
     * @return $this
     */
    public function calcSupport(array $itemSet)
    {
        $totalTransactions = $this->getTransactionSetSize();
        foreach ($itemSet as $item) {
            $item->support = $item->count / $totalTransactions;
        }

        return $this;
    }

    /**
     * Filters the list of items from F1 that satisfy the minimum support threshold.
     *
     * @return self
     */
    public function filterFrequentItems()
    {
        if (empty($this->frequentItems)) {
            $this->frequentItems = $this->filterItemSet($this->itemsFrequency);
        }

        return $this;
    }

    /**
     * Filter a set of items if its support its below the threshold
     *
     * @param array $itemSet
     * @return array
     */
    public function filterItemSet(array $itemSet)
    {
        return array_values(
            array_filter($itemSet, function ($item) {
                return ($item->support >= $this->minSupp);
            })
        );
    }

    /**
     * Generates the sets of frequent k-items
     * @return self
     */
    public function generateFrequentSets()
    {
        // Store F1 frequent set of items in F
        $this->frequentSets[1] = $this->frequentItems;

        for ($k = 2; !empty($this->frequentSets[$k - 1]); $k++) {
            $candidates = $this->getCandidates($this->frequentSets[$k - 1]);
            foreach ($this->transactionCollection as $transaction) {
                foreach ($candidates as $candidate) {
                    if ($transaction->contains($candidate->value)) {
                        $candidate->count++;
                    }
                }
            }

            // Calculate each candidate's support.
            $this->calcSupport($candidates);

            // Filter candidates below the minimum support
            $candidates = $this->filterItemSet($candidates);
            array_push($this->frequentSets, $candidates);
        }

        return $this;
    }

    /**
     * Check if a given element is inside an array.
     *
     * @param mixed $a First Item
     * @param mixed $b Second Item
     * @return bool
     */
    public function contains($a, $b)
    {
        $reducer = function ($set, $value) {
            foreach ($set as $elm) {
                if ($elm === $value) {
                    return true;
                }
            }

            return false;
        };

        return $reducer($a, $b);
    }

    /**
     * Generates candidates for a given frequent set of items.
     * @param $frequentItemSet
     * @return array
     */
    public function getCandidates($frequentItemSet)
    {
        $candidates = array();
        $frequentItemsSize = count($frequentItemSet);
        for ($i = 0; $i < $frequentItemsSize; $i++) {
            for ($j = $i + 1; $j < $frequentItemsSize; $j++) {
                $setsDiff = array_merge(
                    array_diff($frequentItemSet[$i]->value, $frequentItemSet[$j]->value),
                    array_diff($frequentItemSet[$j]->value, $frequentItemSet[$i]->value)
                );

                // If two items differs in more than two elements, then skip the candidate generation.
                if (count($setsDiff) != 2) {
                    continue;
                }

                // Get unique values after merging the two arrays.
                $candidate = array_unique(array_merge($frequentItemSet[$i]->value, $frequentItemSet[$j]->value));
                // Sort and reset the numeric keys from 0 to N to avoid issues while comparing arrays with same values
                // but different keys.
                sort($candidate);
                if (!$this->contains($candidates, $candidate)) {
                    array_push($candidates, $candidate);
                }
            }
        }

        // Turn candidates into objects so each $candidate has its own support calculation.
        foreach ($candidates as $key => $candidate) {
            $item = new ItemModel($candidate);
            $candidates[$key] = $item;
        }

        return $candidates;
    }

    /**
     * Generates 1-item consequent rules
     *
     * @param array $F Frequent Set
     */
    public function generateRules()
    {
        $genRules = function ($itemSet) {
            $rules = array();
            foreach ($itemSet as $item) {
                $length = count($item->value);
                for ($i = 0; $i < $length; $i++) {
                    $items = $item->value;
                    $rule = new RuleModel();
                    // Consequent needs to be un array also.
                    $rule->consequent = array($items[$i]);
                    unset($items[$i]);
                    $rule->precedent = $items;
                    array_push($rules, $rule);
                }
            }

            return $rules;
        };

        // Generar todas las reglas con un consequente a partir de F2,
        // Agarrar los consequentes de esas reglas generadas y dejar solo las que superan el minimo de conf.
        // Almacenar esos consequentes en H.
        // Generar candidatos con H1, y asi sucesivamente.
        $length = count($this->frequentSets);
        for ($k = 2; $k <= $length; $k++) {
            $H = array();
            if (!empty($this->frequentSets[$k])) {
                $rules = $genRules($this->frequentSets[$k]);
                foreach ($rules as $key => $rule) {
                    $rule->confidence = $this->getConfidence($this->frequentSets, $rule, $k);
                    if ($rule->confidence >= $this->minConf) {
                        array_push($this->rules, $rule);
                        array_push($H, $rule->consequent);
                    }
                }
            }
            $this->generateRulesRecursively($this->frequentSets[$k], $H);
        }
        $this->sort($this->rules, 'precedent');
    }

    /**
     * Calculates the confidence of a rule.
     *
     * @param array $F All the frequent sets
     * @param RuleModel $rule The rule to get its confidence calculated
     * @param integer $k The subindex of the Fk item set being analyzed.
     * @return float|int
     */
    public function getConfidence($F, $rule, $k)
    {
        $ruleCount = 0;
        $precedentCount = 0;
        foreach ($F[$k - 1] as $item) {
            if (empty(array_diff($item->value, $rule->precedent))) {
                $precedentCount = $item->count;
                break;
            }
        }

        foreach (($F[$k]) as $item) {
            if (empty(array_diff($item->value, array_merge($rule->consequent, $rule->precedent)))) {
                $ruleCount = $item->count;
                break;
            }
        }

        return ($precedentCount !== 0) ? $ruleCount / $precedentCount : 0;
    }

    /**
     * @param $frequentSet
     * @param ItemModel[] $H The list of consequent k-items that are above the min conf threshold
     */
    public function generateRulesRecursively($frequentSet, $H)
    {
        $rules = array();
        $K = isset($frequentSet[0]) ? count($frequentSet[0]) : 0;
        $M = isset($H[0]) ? count($H[0]) : 0;
        if ($K > $M + 1 && !empty($H)) {
            /**
             * @var $Hnext ItemModel[]
             */
            $Hnext = $this->getCandidates($H);
            foreach ($Hnext as $key => $hItem) {
                $storeHItem = false;
                foreach ($frequentSet[$K] as $item) {
                    if (array_diff($hItem->value, $item) === 0) {
                        $rule = new RuleModel();
                        $rule->consequent = $hItem->value;
                        $rule->precedent = array_diff($frequentSet[$K], $hItem->value);
                        $rule->confidence = $this->getConfidence($this->frequentSets, $rule, $K);
                        if ($rule->confidence >= $this->minConf) {
                            array_push($rules, $rule);
                            $storeHItem = true;
                        }
                    }
                }
                if (!$storeHItem) {
                    unset($Hnext[$key]);
                }
            }
            $this->sort($rules, 'precedent');
            $this->rules = array_merge($this->rules, $rules);

            $this->generateRulesRecursively($frequentSet[$K], $Hnext);
        }
    }
}

?>