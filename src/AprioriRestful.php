<?php
ini_set('memory_limit', -1);
set_time_limit(0);

require_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

use IAFRRe\Apriori\Apriori;
use IAFRRe\Apriori\Transaction\Adapter\FilesystemAdapter;

// Constants definitions
define('APPLICATION_PATH', __DIR__);
define('TRANSACTION_FILES_PATH', APPLICATION_PATH . '/../db/');
$request = Request::createFromGlobals();

if ($request->isMethod(Request::METHOD_POST)) {
    $form = $request->request->all();
    /**
     * @var $file \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    $file = $request->files->get('fileSubmission');

    try {
        if (isset($file) && $file->isFile()) {
            if (file_exists(TRANSACTION_FILES_PATH . $file->getClientOriginalName())) {
                unlink(TRANSACTION_FILES_PATH . $file->getClientOriginalName());
            }
            $file->move(TRANSACTION_FILES_PATH, $file->getClientOriginalName());
            $fileName = $file->getClientOriginalName();
        } elseif (isset($form['uploadedDocuments']) && $form['uploadedDocuments'] !== 'none') {
            $fileName = $form['uploadedDocuments'];
        } else {
            throw new Exception(
                'Por favor elija un archivo de transacciones existente del servidor o bien cargue uno nuevo.',
                404
            );
        }
        $transactionsProcessor = new FilesystemAdapter(TRANSACTION_FILES_PATH . $fileName);
        $transactions = $transactionsProcessor->getTransactions();

        $apriori = new Apriori();
        $apriori
            ->setMinSupp($form['minSupp'])
            ->setMinConf($form['minConf'])
            ->setTransactionCollection($transactions);

        $apriori->run();

        $response = new \stdClass();
        $response->data = array();
        $response->minConfidence = $apriori->getMinConf() * 100;
        $response->minSupport = $apriori->getMinSupp() * 100;
        $response->totalRules = count($apriori->rules);
        foreach ($apriori->rules as $rule) {
            $precedent = implode(', ', $rule->precedent);
            $consequent = implode(', ', $rule->consequent);
            $serializedRule = new \stdClass();
            $serializedRule->rule = sprintf('%s => %s', $precedent, $consequent);
            $serializedRule->confidence = $rule->confidence * 100;
            array_push($response->data, $serializedRule);
        }

        header('Content-Type: application/json');
        http_response_code(200);
        echo json_encode($response);

    } catch (Exception $e) {
        $errorMessage = $e->getCode() === 404
            ? $e->getMessage()
            : 'Se ha producido un error al intentar procesar su solicitud. Verifique los campos formulario e intente nuevamente.';
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array(
            'error' => $errorMessage,
            'data' => array(),
            'support' => $form['minSupp'],
            'confidence' => $form['minConf']
        ));
    }
}