(function ($) {
    $(document).ready(function () {
        $('#aprioriConfiguration')
            .submit(function (e) {
                var that = this;
                e.preventDefault();
                var minSupp = $(this).find('#minSupp').val(),
                    minConf = $(this).find('#minConf').val(),
                    noFiles = $(this).find('#fileSubmission').get(0).files.length === 0,
                    noUploadedDocuments = $(this).find('#uploadedDocuments option:selected').val() === 'none',
                    errorMsg = [];


                if (noFiles && noUploadedDocuments) {
                    errorMsg.push('Por favor, seleccione un origen de datos válido.');
                }
                if (minSupp > minConf) {
                    errorMsg.push('El parámetro de confianza no puede ser menor al soporte.');
                }

                if (noFiles && noUploadedDocuments || minSupp > minConf) {
                    var $alert = $('#errorMsg');
                    if ($alert.length > 0) {
                        $alert.remove();
                    }
                    $alert = $('<div>', {
                        id: 'errorMsg',
                        class: 'alert alert-danger alert-dismissible fade show',
                        role: 'alert'
                    });
                    $dismissButton = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                    $alert.append($dismissButton);
                    $alert.alert();
                    $errorList = $('<ul>');
                    $.each(errorMsg, function () {
                        $msg = this;
                        $errorItem = $('<li>').append(this);
                        $errorList.append($errorItem);
                    });
                    $alert.append($errorList);
                    $('body').prepend($alert);
                    return false;
                }
                $('#aprioriModal').modal('show');
                $('#aprioriTable').DataTable({
                    ajax: function (data, callback, settings) {
                        $.ajax({
                            url: '/AprioriRestful.php',
                            type: 'POST',
                            data: new FormData(that),
                            processData: false,
                            contentType: false
                        }).done(function (data, textStatus, jqXHR) {
                            callback(data);
                            $('.min-supp').html(data.minSupport);
                            $('.min-conf').html(data.minConfidence);
                            $('.total-rules').html(data.totalRules);
                            $('#tableMetadata').toggleClass('hidden');
                        }).fail(function () {

                        });
                    },
                    destroy: true,
                    order: [
                        [1, 'desc']
                    ],
                    dom: '<"row"<"col-sm-12 col-md-6"B>><"row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>rt<"row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    buttons: [
                        'csv',
                        'excel',
                        'pdf', {
                            extend: 'print',
                            text: 'Imprimir'
                        }, {
                            text: 'Modo Lectura',
                            action: function (e, dt, $button, config) {
                                $('#aprioriTable').toggleClass('table-dark');
                                $button.toggleClass('pressed');
                            }
                        }
                    ],
                    paging: true,
                    pageLength: 10,
                    language: {
                        search: 'Filtrar Reglas',
                        searchPlaceholder: '',
                        emptyTable: 'No se ha generado ninguna regla. Por favor intenta nuevamente con otros parámetros de Soporte y Confianza.',
                        info: 'Mostrando página _PAGE_ de _PAGES_ - Cantidad de Reglas: _TOTAL_/_MAX_',
                        lengthMenu: 'Resultados por página _MENU_',
                        infoFiltered: '',
                        infoEmpty: 'No hay resultados para mostrar',
                        infoPostFix: '',
                        thousands: '.',
                        loadingRecords: '<span class="spinner" aria-hidden="true"></span>Procesando Reglas...',
                        processing: 'Cargando resultados...',
                        decimal: ',',
                        paginate: {
                            first: 'Primera Página',
                            last: 'Última Página',
                            next: 'Siguiente',
                            previous: 'Anterior'
                        },
                        aria: {
                            sortAscending: ': activar para ordenar la columna en forma ascendente',
                            sortDescending: ': activar para ordenar la columna en forma descendente'
                        },
                        zeroRecords: 'No se han encontrado resultados.'
                    },
                    lengthMenu: [5, 10, 25, 50, 75, 100],
                    columns: [
                        {
                            data: 'rule',
                            width: '75%'
                        },
                        {
                            data: 'confidence',
                            searchable: false,
                            render: $.fn.dataTable.render.number('.', ',', 2, '', '%')
                        }
                    ]
                });
            });
        $('#aprioriModal')
            .on('hidden.bs.modal', function (e) {
                $('#tableMetadata').toggleClass('hidden');
                $('#aprioriTable').toggleClass('table-dark');
            });

    });
}(jQuery));