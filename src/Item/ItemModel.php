<?php

namespace IAFRRe\Apriori\Item;


class ItemModel
{
    /**
     * @var mixed
     */
    public $value;

    /**
     * @var int
     */
    public $count;

    /**
     * @return float
     */
    public $support;

    public function __construct(array $value = array(), $count = 0, $support = 0)
    {
        $this->value = $value;
        $this->count = $count;
        $this->support = $support;
    }

    public function __toString()
    {
        return sprintf(
            "[ %s ] - Soporte: %01.2f\n",
            implode(' ', $this->value),
            $this->support
        );
    }
}