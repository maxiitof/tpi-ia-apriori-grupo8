<?php
/**
 * Created by PhpStorm.
 * User: mfont
 * Date: 6/11/2017
 * Time: 02:41
 */

namespace IAFRRe\Apriori\Rule;


use IAFRRe\Apriori\Item\ItemModel;

class RuleModel
{
    /**
     * @var ItemModel[]
     */
    public $consequent;

    /**
     * @var ItemModel[]
     */
    public $precedent;

    /**
     * @var float
     */
    public $confidence;

    public function __construct($precedent = array(), $consequent = array())
    {
        $this->consequent = !is_array($consequent) ? array($consequent) : $consequent;
        $this->precedent = $precedent;
    }

    public function __toString()
    {
        return sprintf(
            "%s => %s - Confianza: %01.2f\n",
            implode(' ', $this->precedent),
            implode('', $this->consequent),
            $this->confidence
        );
    }
}