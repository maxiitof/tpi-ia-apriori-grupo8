<?php

namespace IAFRRe\Apriori\Transaction;

use function array_intersect;
use function count;

class TransactionModel implements \Iterator
{
    /**
     * An array of transaction items.
     * @var mixed[]
     */
    protected $items;

    /**
     * @var int
     */
    private $position;

    /**
     * Transaction constructor.
     *
     * @param string $transaction
     * @param string $delimiter
     * @param bool $sort
     * @param int $sortType
     *
     * @throws \Exception
     */
    public function __construct($items)
    {
        $this->items = $items;
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $items
     * @return bool
     */
    public function contains($items)
    {
        return count(array_intersect($this->items, $items)) === count($items);
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->items[$this->position];
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return isset($this->items[$this->position]);
    }
}