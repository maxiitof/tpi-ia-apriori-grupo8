<?php

namespace IAFRRe\Apriori\Transaction\Adapter;

use function array_push;
use Exception;
use function explode;
use function gettype;
use IAFRRe\Apriori\Transaction\TransactionModel;
use function is_string;
use const PHP_EOL;
use const SORT_STRING;
use function sprintf;
use function strlen;

class FilesystemAdapter extends AbstractAdapter
{
    /**
     * @var string
     */
    protected $delimiter;

    /**
     * @var \SplFileObject
     */
    protected $file;

    /**
     * @var bool
     */
    protected $sort;

    /**
     * @var int
     */
    protected $sortType;


    /**
     * @var int[]
     */
    protected static $sortTypes = array(
        SORT_LOCALE_STRING,
        SORT_NATURAL,
        SORT_NUMERIC,
        SORT_STRING,
        SORT_REGULAR,
    );

    public function __construct(string $path, string $delimiter = ' ', bool $sort = false, int $sortType = SORT_NUMERIC)
    {
        $this->delimiter = $delimiter;
        $this->sort = $sort;
        $this->sortType = $sortType;

        $this->file = new \SplFileObject($path);
        if (!$this->file->isFile()) {
            throw new \Exception(sprintf(
                'Error while looking for the file. The path provided is not a valid file. Path provided: "%s"',
                $path
            ));
        }

        if (!$this->file->isReadable()) {
            throw new \Exception(sprintf(
                'File "%s%" is not readable. Please check permissions.',
                $this->file->getFilename()
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function processRawTransaction($rawData)
    {
        if (!is_string($rawData)) {
            throw new \Exception(sprintf(
                '%s::%s expects a string with data in a raw format. Type %s given.',
                self::class,
                __METHOD__,
                gettype($rawData)
            ));
        }

        //Handle transaction instantiation
        $items = explode(' ', trim($rawData));
        if ($this->sort) {
            if (in_array($this->sortType, self::$sortTypes)) {
                sort($items, $this->sortType);
            } else {
                throw new \Exception(sprintf(
                    '%s::%s expects a valid SORT TYPE; received %s.',
                    self::class,
                    __METHOD__,
                    $this->sortType
                ));
            }
        }

        $items = $this->normalizeTransactions($items);

        // Do not instantdiate an empty transaction that can affect support and confidence measurement
        if (empty($items)) {
            throw new \Exception(sprintf(
                'Trying to create an empty transaction in %s::%s.',
                self::class,
                __METHOD__,
                $this->sortType
            ));
        }

        $transaction = new TransactionModel($items);

        return $transaction;
    }

    protected function normalizeTransactions($items)
    {
        $items = array_unique($items);
        return array_values(
            array_filter($items, function ($item) {
                return ($item !== ' ');
            })
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactions()
    {
        $transactions = array();
        while (!$this->file->eof()) {
            try {
                $line = $this->file->fgets();
                array_push($transactions, $this->processRawTransaction($line));
            } catch (Exception $e) {
                //@TODO Handle Exception
                echo $e->getMessage() . PHP_EOL;
            }
        };

        return $transactions;
    }

    /**
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter)
    {
        $this->delimiter = $delimiter;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSort(): bool
    {
        return $this->sort;
    }

    /**
     * @param bool $sort
     */
    public function setSort(bool $sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return int
     */
    public function getSortType(): int
    {
        return $this->sortType;
    }

    /**
     * @param int $sortType
     */
    public function setSortType(int $sortType)
    {
        $this->sortType = $sortType;

        return $this;
    }


}