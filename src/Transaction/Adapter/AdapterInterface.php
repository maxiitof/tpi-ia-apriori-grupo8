<?php

namespace IAFRRe\Apriori\Transaction\Adapter;

use IAFRRe\Apriori\Transaction\TransactionModel;

interface AdapterInterface
{

    /**
     * Returns a set of transactions from a data source
     *
     * @return TransactionModel[]
     */
    public function getTransactions();

    /**
     * Processes raw data and returns a transaction object
     *
     * @param mixed $rawData Data in a row format that this method process to instantiate a transaction.
     * @return TransactionModel
     */
    public function processRawTransaction($rawData);
}