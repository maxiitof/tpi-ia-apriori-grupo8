<?php
ini_set("memory_limit", -1);
set_time_limit(0);

require_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

// Constants definitions
define('APPLICATION_PATH', __DIR__);
define('TRANSACTION_FILES_PATH', APPLICATION_PATH . '/../db/');
$request = Request::createFromGlobals();

$renderResults = null;
$errorMessage = null;

$transactionFiles = array_diff(scandir(TRANSACTION_FILES_PATH), array('..', '.', '.gitkeep'));
?>

<html>
<head>
    <title>Apriori | Grupo 08 | FRRe</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS Assets -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="assets/css/apriori.css" type="text/css" media="all">
</head>
<body class="container-fluid">
<h1 id="pageTitle" class="title-1">Algoritmo Apriori - UTN FRRe - Grupo 8</h1>
<form id="aprioriConfiguration" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="fileSubmission">Subí tu archivo con transacciones</label>
        <input id="fileSubmission" name="fileSubmission" type="file">
        <small id="minConfDescription" class="form-text text-muted">Archivos con el mismo nombre serán reemplazados
            en
            el sistema de archivos del servidor.
        </small>
    </div>
    <div class="form-group">
        <label for="uploadedDocuments">O elegí un documento ya disponible</label>
        <select class="form-control" id="uploadedDocuments" name="uploadedDocuments">
            <option value="none" selected>Ninguno</option>
            <?php foreach ($transactionFiles as $fileName) { ?>
                <option value="<?php echo $fileName; ?>">
                    <?php echo $fileName; ?>
                </option>
            <?php } ?>
        </select>
        <small id="minConfDescription" class="form-text text-muted">En caso de adjuntar un archivo, este campo no
            tendrá
            efecto.
        </small>
    </div>
    <div class="form-group">
        <label for="minSupp">Mínimo Soporte</label>
        <input type="number" class="form-control" id="minSupp" name="minSupp" aria-describedby="minSuppDescription"
               placeholder="Ingresa un porcentaje entre 1 y 100" step="0.01" min="1" max="100" value="30">
        <small id="minSuppDescription" class="form-text text-muted">Enteros válidos entre entre 1 y 100</small>
    </div>
    <div class="form-group">
        <label for="minConf">Mínimo Confianza</label>
        <input type="number" class="form-control" id="minConf" name="minConf" aria-describedby="minConfDescription"
               placeholder="Ingresa un porcentaje o clickea en las flechitas para incrementar" step="0.01" min="1"
               max="100" value="80">
        <small id="minConfDescription" class="form-text text-muted">Enteros válidos entre entre 1 y 100</small>
    </div>
    <button type="submit" class="btn btn-primary" id="submitForm" name="submitForm">Calcular</button>
</form>
<div class="modal fade" id="aprioriModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Apriori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row hidden" id="tableMetadata">
                    <div class="col-sm-8 col-md-4">
                        <b>Soporte: </b><span class="min-supp"></span>%
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <b>Confianza: </b><span class="min-conf"></span>%
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <b>Reglas: </b><span class="total-rules"></span>
                    </div>
                </div>
                <table id="aprioriTable" class="table table-striped table-responsive-sm" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th scope="col">Regla</th>
                        <th scope="col">Confianza</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <a class="btn btn-primary" href="/">Probar otro dataset</a>
            </div>
        </div>
    </div>
</div>
<footer id="footer" class="container-fluid">
    <hr class="featurette-divider">
    <div class="container">
        <p class="float-right"><a href="#pageTitle">Ir arriba</a></p>
        <p>
            &copy; 2017 UTN FRRe · Inteligencia Artificial</a>
        </p>
    </div>
</footer>
<!-- CSS Assets -->
<link rel="stylesheet" href="assets/css/buttons.dataTables.min.css" type="text/css" media="all">
<link rel="stylesheet" href="assets/css/jquery.dataTables.min.css" type="text/css" media="all">
<!-- JS Assets -->
<script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/popper-1.12.3.min.js"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/js/dataTables.select.min.js" type="text/javascript"></script>
<script src="assets/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="assets/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="assets/js/buttons.print.min.js" type="text/javascript"></script>
<script src="assets/js/pdfmake.min.js" type="text/javascript"></script>
<script src="assets/js/vfs_fonts.js" type="text/javascript"></script>
<script src="assets/js/apriori.js"></script>
</body>
</html>
